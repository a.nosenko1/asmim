#include <stdio.h>
#include <stdint.h>
#include <x86intrin.h>

#define REPEATS 100000000

int main(int argc, char* argv[]) {
  	unsigned long long start = 0;
  	unsigned long long end = 0;
  	volatile int a = 0;
	
  	FILE* file = fopen("result_step_1.txt", "w");
  	fprintf(file, "K, cycles\n");
	printf("K, cycles\n");

 	for (register int K = 1; K <= 120; K++) {
  		start = __rdtsc();
  		for (register int i = 0; i < REPEATS; i++) {
  			if ((i % K) == 0) a = 1;
 	 	}
  		end = __rdtsc();
 		printf("%d, %.4lf\n", K, (double)(end - start) / REPEATS);
 	 	fprintf(file, "%d, %.4lf\n", K, (double)(end - start) / REPEATS);
 	}
  	fclose(file);

  return 0;
}

