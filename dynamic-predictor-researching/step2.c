#include <stdio.h>
#include <stdint.h>
#include <x86intrin.h>

#define REPEATS 100000000

#define FALSE_BRANCH if (i < 0) a = 1;
#define FALSE_BRANCH2 FALSE_BRANCH FALSE_BRANCH
#define FALSE_BRANCH4 FALSE_BRANCH2 FALSE_BRANCH2
#define FALSE_BRANCH8 FALSE_BRANCH4 FALSE_BRANCH4
#define FALSE_BRANCH16 FALSE_BRANCH8 FALSE_BRANCH8
#define FALSE_BRANCH32 FALSE_BRANCH16 FALSE_BRANCH16
#define FALSE_BRANCH64 FALSE_BRANCH32 FALSE_BRANCH32


int main(int argc, char* argv[]) {
    unsigned long long start = 0;
    unsigned long long end = 0;
    unsigned long long start1 = 0;
    unsigned long long end1 = 0;
    unsigned long long onlyOne = 0;
    volatile int a = 0;
    
    FILE* file = fopen("result_step_1.txt", "w");
    fprintf(file, "K, cycles\n");
    printf("K, cycles\n");

    for (register int K = 20; K <= 100; K++) {
        for (int i1 = 0; i1 < 1; ++i1)
        {
            onlyOne = 0;
            start = __rdtsc();
            for (register int i = 0; i < REPEATS; i++) {
                FALSE_BRANCH64
                FALSE_BRANCH32
                start1 = __rdtsc();
                if ((i % K) == 0) a = 1;
                end1 = __rdtsc();
                onlyOne += end1 - start1;
            }
            end = __rdtsc();
            printf("%d %.4lf %.4lf\n", K, (double)(end - start) / REPEATS, (double)(onlyOne) / REPEATS);
            fprintf(file, "%d, %.4lf\n", K, (double)(end - start) / REPEATS);
        }
        
    }
    fclose(file);

    return 0;
}

